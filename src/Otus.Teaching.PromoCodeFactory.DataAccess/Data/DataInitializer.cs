﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class DataInitializer : IDbInitializer
    {
        private readonly UserContext db;

        public DataInitializer(UserContext db)
        {
            this.db = db;
        }

        public void Init()
        {
            db.Database.EnsureDeleted();
            db.Database.EnsureCreated();
            db.AddRange(FakeDataFactory.Roles);
            db.AddRange(FakeDataFactory.Customers);
            db.AddRange(FakeDataFactory.Preferences);
            db.AddRange(FakeDataFactory.Employees);
            db.SaveChanges();
        }
    }
}