﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class FakeDataFactory
    {
        private static readonly Employee Employee2 = new Employee()
        {
            Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
            Email = "andreev@somemail.ru",
            FirstName = "Петр",
            LastName = "Андреев",
            Role = rolemanager,
            AppliedPromocodesCount = 10
        };
        public static IEnumerable<Employee> Employees => new List<Employee>()
        {
            new Employee()
            {
                Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                Role = roleAdmin,
                AppliedPromocodesCount = 5
            },
            Employee2,
        };

        private static readonly Role roleAdmin = new Role()
        {
            Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
            Name = "Admin",
            Description = "Администратор",
        };
        private static readonly Role rolemanager = new Role()
        {
            Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
            Name = "PartnerManager",
            Description = "Партнерский менеджер"
        };

        public static IEnumerable<Role> Roles => new List<Role>()
        {
            roleAdmin,
            rolemanager
        };

        private static readonly Preference Preference1 = new Preference()
        {
            Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
            Name = "Театр",
        };
        public static IEnumerable<Preference> Preferences => new List<Preference>()
        {
            Preference1,
            new Preference()
            {
                Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                Name = "Семья",
            },
            new Preference()
            {
                Id = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                Name = "Дети",
            }
        };

        public static IEnumerable<Customer> Customers
        {
            get
            {
                var customerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0");
                var customers = new List<Customer>()
                {
                    new Customer()
                    {
                        Id = customerId,
                        Email = "ivan_sergeev@mail.ru",
                        FirstName = "Иван",
                        LastName = "Петров",
                        //TODO: Добавить предзаполненный список предпочтений (Done)
                        CustomerPreferences = new List<CustomerPreference>
                        {
                            new CustomerPreference
                            {
                                CustomerId = customerId,
                                PreferenceId = Preferences.First().Id,
                            }
                        },
                        PromoCodes = new List<PromoCode>
                        {
                            new PromoCode
                            {
                                Code = "промо1",
                                BeginDate = DateTime.Now,
                                EndDate = DateTime.Now.AddDays(2),
                                Id = Guid.Parse("a1503c06-dc90-41b2-ab49-0179fe1a5fb6"),
                                PartnerManager = Employee2,
                                PartnerName = "ооо рога и коп",
                                Preference = Preference1,
                                ServiceInfo = "serviceInfo 1",
                            }
                        }
                    }
                };

                return customers;
            }
        }
    }
}