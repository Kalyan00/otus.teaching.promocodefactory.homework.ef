﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T> : IDisposable
        where T : BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();

        Task<T> GetByIdAsync(Guid id);
        Task Create(T entity);
        Task Update(T entity);
        Task Delete(T entity);
        Task Delete(IEnumerable<T> entities);
        Task Delete(Guid id);
        Task<IEnumerable<T>> WhereAsync(System.Linq.Expressions.Expression<Func<T, bool>> predicate);
        Task Save();
    }
}