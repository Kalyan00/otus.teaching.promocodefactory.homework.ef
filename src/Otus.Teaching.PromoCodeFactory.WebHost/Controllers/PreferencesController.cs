﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController
        : ControllerBase
    {
        private readonly IRepository<Preference> preferenceRepository;

        public PreferencesController(IRepository<Preference> preferenceRepository)
        {
            this.preferenceRepository = preferenceRepository;
        }
        /// <summary>
        /// Получить все предпочтения
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<List<PrefernceResponse>>> GetPromocodesAsync()
        {
            return (await preferenceRepository.GetAllAsync())
                .Select(x => new PrefernceResponse
                {
                    PreferenceId = x.Id,
                    Name = x.Name,
                })
                .ToList();
        }
    }
}