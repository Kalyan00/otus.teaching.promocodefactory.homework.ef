﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> customerRepository;
        private readonly IRepository<Preference> preferenceRepository;
        private readonly IRepository<PromoCode> promoCodeRepository;

        public CustomersController(
            IRepository<Customer> customerRepository, 
            IRepository<Preference> preferenceRepository,
            IRepository<PromoCode> promoCodeRepository)
        {
            this.customerRepository = customerRepository;
            this.preferenceRepository = preferenceRepository;
            this.promoCodeRepository = promoCodeRepository;
        }

        /// <summary>
        /// Получение всех Customer
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            return (await customerRepository.GetAllAsync())
                .Select(x => new CustomerShortResponse
                {
                    Email = x.Email,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Id = x.Id,
                }).ToList();
        }

        /// <summary>
        /// получение клиента вместе с выданными ему промомкодами 
        /// </summary>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            //TODO: Добавить получение клиента вместе с выданными ему промомкодами (DONE)
            var c = await customerRepository.GetByIdAsync(id);
            if(c == null)
                return NotFound();
            return new CustomerResponse
            {
                Email = c.Email,
                FirstName = c.FirstName,
                Id = c.Id,
                LastName = c.LastName,
                PromoCodes = c.PromoCodes.Select(p => new PromoCodeShortResponse
                {
                    Code = p.Code,
                    Id = p.Id,  
                    BeginDate = p.BeginDate.ToString(),
                    EndDate = p.EndDate.ToString(),
                    PartnerName = p.PartnerName,
                    ServiceInfo = p.ServiceInfo,
                }).ToList(),
                Prefernces = c.CustomerPreferences.Select(cp=>new PrefernceResponse
                {
                    PreferenceId = cp.PreferenceId,
                    Name = cp.Preference.Name
                }).ToList(),
            };
        }

        /// <summary>
        /// создание нового клиента вместе с его предпочтениями
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            //TODO: Добавить создание нового клиента вместе с его предпочтениями (DONE)
            
            var customer = new Customer { Id = Guid.NewGuid() };
            await MapCustomer(customer, request);
            await customerRepository.Create(customer);
            return Ok();
        }

        /// <summary>
        /// Обновить данные клиента вместе с его предпочтениями
        /// </summary>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            //TODO: Обновить данные клиента вместе с его предпочтениями(Done)
            
            var customer = await customerRepository.GetByIdAsync(id);
            if (customer == null)
                return NotFound();

            await MapCustomer(customer, request);
            await customerRepository.Update(customer);
            return Ok();
        }

        private async Task MapCustomer(Customer customer, CreateOrEditCustomerRequest request)
        {
            customer.Email = request.Email;
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;


            if (request.PreferenceIds != null)
            {
                customer.CustomerPreferences?.Clear();
                customer.CustomerPreferences =
                    (await preferenceRepository.GetAllAsync())
                    .Where(p => request.PreferenceIds.Contains(p.Id))
                    .Select(p => new CustomerPreference
                    {
                        CustomerId = customer.Id,
                        PreferenceId = p.Id
                    })
                    .ToList();
            }

        }

        /// <summary>
        /// Удаление клиента вместе с выданными ему промокодами
        /// </summary>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            //TODO: Удаление клиента вместе с выданными ему промокодами (DONE)
            var customer = await customerRepository.GetByIdAsync(id);
            if (customer == null)
                return NotFound(id);
            await promoCodeRepository.Delete(customer.PromoCodes);
            await customerRepository.Delete(customer);
            return Ok();
        }
    }
}