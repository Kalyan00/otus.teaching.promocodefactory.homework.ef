﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> promoCodeRepository;
        private readonly IRepository<Customer> customerRepository;
        private readonly IRepository<Preference> preferenceRepository;

        public PromocodesController(
            IRepository<PromoCode> promoCodeRepository, 
            IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            this.promoCodeRepository = promoCodeRepository;
            this.customerRepository = customerRepository;
            this.preferenceRepository = preferenceRepository;
        }
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            return (await promoCodeRepository.GetAllAsync())
                .Select(x=>new PromoCodeShortResponse
                {
                    Id = x.Id,
                    BeginDate = x.BeginDate.ToString(),
                    Code = x.Code,
                    EndDate = x.EndDate.ToString(),
                    PartnerName = x.PartnerName,
                    ServiceInfo = x.ServiceInfo,
                })
                .ToList();
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {

            //TODO: Создать промокод и выдать его клиентам с указанным предпочтением
            var pref = (await preferenceRepository.WhereAsync(p=>p.Name == request.Preference)).FirstOrDefault();
            if (pref == null)
                return NotFound(request.Preference);

           

            foreach(var cust in (await customerRepository.WhereAsync(c=>c.CustomerPreferences.Any(p=>p.PreferenceId == pref.Id))).ToArray())
            {
                cust.PromoCodes.Add(new PromoCode
                {
                    Id = Guid.NewGuid(),
                    BeginDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(30),
                    Code = request.PromoCode,
                    PartnerName = request.PartnerName,
                    ServiceInfo = request.ServiceInfo,
                    Preference = pref,

                });
                await customerRepository.Update(cust);
            }
            //await customerRepository.Save();
            return Ok();
        }
    }

}