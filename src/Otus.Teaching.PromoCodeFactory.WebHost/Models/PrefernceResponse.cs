﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PrefernceResponse
    {
        public Guid PreferenceId { get; internal set; }
        public string Name { get; internal set; }
    }
}